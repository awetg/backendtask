const update = document.getElementById('update');
const signUp = document.getElementById('signUp');
const logIn = document.getElementById('logIn');

const setCookie = (key, value,days) => {
	let expires;
	if(days) {
		expires = new Date(new Date().getTime() + days*24*60*60*1000);
	} else{
		expires = '';
	}
document.cookie = `${encodeURIComponent(key)}=${encodeURIComponent(value)};expires=${expires}`;
}

const getCookie = key => decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;

message.style.color = 'red';
const cookieSet = getCookie('cookieName');
if(cookieSet) {
  signUp.style.display = 'none';
  logIn.innerText = 'Logout';
}

logIn.addEventListener('click',(evt) => {
	if(cookieSet) {
		setCookie('cookieName','', -1);
		logIn.innerText='Login';
	}
})