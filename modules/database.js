'use strict';
const mysql = require('mysql2');
const jwt = require('jsonwebtoken');
const jwtBlackList = [];
const jwtSecretKey = 'secret key';


const connect = () => {
    // create the connection to database
    const connection = mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        database: process.env.DB_DATABASE,
        password: process.env.DB_PASSWORD
    });
    return connection;
};
const select = (connection, callback, res) => {
    // simple query
    connection.query(
        'SELECT * FROM bc_media',
    (err, results, fields) => {
    //   console.log(results); // results contains rows returned by server
    //   // console.log(fields); // fields contains extra meta data about results, if available
    //   console.log(err);
        callback(results, res);
    }
)};

const insertMedia = (data, connection) => {
    // simple query
    return new Promise((resolve, reject) => {
        try {
            connection.execute(
                'INSERT INTO bc_media (category, title, details, coordinates, thumbnail, image, original) VALUES (?, ?, ?, ?, ?, ?, ?);', data,
            (err, results, fields) => {
                err? reject(err) : resolve(results);
            });
         } catch(error) {
            reject(error)
         }
     });
};

const updateMedia = (data, connection) => {
    return new Promise((resolve, reject) => {
        try  {
            const query = 'UPDATE bc_media SET category= ?, title=?, details=?, coordinates=?, thumbnail=?, image=?, original=? WHERE id=?;';
            connection.execute(query, data, (error, results, fields) => {
                error? reject(error) : resolve(results);
            });
        } catch(error) {
            reject(error)
        }
    });
};

const deleteMedia = (data, connection) => {
    return new Promise((resolve, reject) => {
        try {
            connect.execute('DELETE FROM bc_media WHERE id=?', data, (error,results, fields) => {
                error? reject(error) : resolve(results);
            });
        } catch(error) {
            reject(error);
        }
    });
};

const insertUser = (data, connection) => {
    return new Promise((resolve, reject) => {
        try {
            connection.execute(
                'INSERT INTO user (username, password) VALUES (?, ?);', data,
                (err, results, fields) => {
                    err? reject(err) : resolve(results);
                });
        } catch(error) {
            reject(error)
        }
    });
}

const logIn = (data, connection) => {
    return new Promise((resolve, reject) => {
        try {
            connection.execute(
                'SELECT * FROM user WHERE username=? AND password=?', data,
                (error, results, fields) => {
                    if(error || !results[0]) {
                        error? reject(error) : reject(results);
                    } else {
                        const payload = {exp: Math.floor(Date.now() / 1000) + (60 * 60),data: results[0]};
                        jwt.sign(payload, jwtSecretKey, (error, token) => {
                            error? reject(error) : resolve(token);
                        });
                    }
                });
        } catch(error) {
            reject(error)
        }
    });
};

const logOut = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, jwtSecretKey, (error, userData) => {
            if(error) {
                reject(error);
            } else {
                jwtBlackList.push(token);
                resolve({message: 'You are logged out.'});
            }
        })
    });
};

const userIsLogged = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, jwtSecretKey, (error, userData) => {
            error? reject(error) : resolve(userData);
        });
    });
};

module.exports = {
    connect : connect,
    select: select,
    insertMedia: insertMedia,
    updateMedia: updateMedia,
    insertUser: insertUser,
    logIn: logIn,
    logOut: logOut,
    userIsLogged: userIsLogged
}