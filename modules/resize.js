const sharp = require('sharp');

const doResize = (pathToFile, width, newPath, next) => {
	sharp(pathToFile)
	.resize(width)
	.toFile(newPath)
	.then(result => console.log('File uploaded'))
	.catch(error => console.log(error));
	next();
}


module.exports = {
	doResize: doResize,
}