# Basic Concepts of Web Applications - Back end task

##How to run

Environment setup:
  * Create new file named `.env` from the `.env_example` file.
  *  To provide secure connection and implement self-signed [SSL](https://www.openssl.org/) the node server is proxed using Apache2.4 full configuration steps can be found on `Task_Config_Files` directory follow the instruction to generate self-signed [SSL](https://www.openssl.org/) certifcate and proxy your server.
  * After that install npm dependecies and run app

    `npm install`

    `node index.js`



  * Open [https://localhost/node/](https://localhost/node/) in browser

## Make a backend to the app in this repo

App features:
  * Add a form to upload data and image
    * Study public/data.json to and add the appropriate fields to the form.
    * You can first set the coordinates by adding two input fields in the form. Later you can do it by clicking (Leaflet etc.) map or get the coordinates from EXIF data.
      * [EXIF](https://github.com/gomfunkel/node-exif)
      * Take pictures with your phone to get images with EXIF-data
  * Upload image
    * send as [FormData](https://developer.mozilla.org/en-US/docs/Web/API/FormData)
    * receive with [_multer_](https://github.com/expressjs/multer)
  * Convert image to small and medium versions
    * [_sharp_](https://github.com/lovell/sharp)
  * Save the data from the form and image urls to database
  * Display the updated data on the frontend
  * Update stored data
  * Delete stored data and files
  * Search entries by some property
  * Authentication
  * https redirection
